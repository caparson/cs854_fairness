import os
import sys
import time
import matplotlib.pyplot as plt
import math
from scipy.special import stdtrit
from scipy import stats
import numpy as np


if len(sys.argv) < 4 or len(sys.argv) > 5:
    print("Not enough args")
    print("Usage: genStats.py Filename NumThreads [PrintCounter]")
    sys.exit()

readfile = open(sys.argv[1], "r")

numThreads = int(sys.argv[2])

writeDirName = sys.argv[3]
writeDir = os.mkdir(writeDirName)

printCounter = False
if len(sys.argv) == 5:
    printCounter = True

histogram = [[0 for i in range(numThreads)] for j in range(numThreads)]
entries = [0] * numThreads
handoffs = [[0 for i in range(numThreads)] for j in range(numThreads)]
counts = [0] * numThreads

started = [False] * numThreads

start = time.time()

last_thd = 0
counter = 0
while True:
    currTime = time.time()
    if (counter % 1000000 == 0 and printCounter):
        print(str(counter) + ", " + str(currTime - start))
    counter += 1

    line = readfile.readline()
    if not line:
        break
    thd = int(line)


    started[thd] = True
    shouldContinue = False
    #skip until all threads have entered at least one to get rid of garbage startup data
    for i in range(numThreads):
        if not started[i]:
            shouldContinue = True
            break

    if shouldContinue:
        continue

    entries[thd] += 1

    handoffs[thd][last_thd] += 1

    while counts[thd] > len(histogram[thd]) - 1:
        histogram[thd].append(0)
    histogram[thd][counts[thd]] += 1
    for i in range(numThreads):
        counts[i] += 1
    counts[thd] = 0

    last_thd = thd

numFilled = [0] * numThreads
for i in range(numThreads):
    for j in histogram[i]:
        if j != 0:
            numFilled[i] += 1

means = [0] * numThreads
stdevs = [0] * numThreads
variances = [0] * numThreads
confidence = [0] * numThreads
cdf = [{} for i in range(numThreads)]

cdf_arr = [[] for j in range(numThreads)]

medians = [-1] * numThreads
percentile75 = [-1] * numThreads
percentile90 = [-1] * numThreads
percentile99 = [-1] * numThreads

for i in range(numThreads):
    currSum = 0
    freqSum = 0
    for idx, j in enumerate(histogram[i]):
        freqSum += j
        if j != 0:
            currSum += j * idx
            
        cdf[i][idx] = freqSum

        cdf_arr[i].append(0)
        cdf_arr[i][idx] = freqSum
            
            
        if currSum > entries[i] * 0.5 and medians[i] == -1:
            medians[i] = idx

        if currSum > entries[i] * 0.75 and percentile75[i] == -1:
            percentile75[i] = idx

        if currSum > entries[i] * 0.9 and percentile90[i] == -1:
            percentile90[i] = idx
        
        if currSum > entries[i] * 0.99 and percentile99[i] == -1:
            percentile99[i] = idx

    means[i] = currSum / entries[i]


    currSum = 0
    for idx, j in enumerate(histogram[i]):
        currSum += j * ((idx - means[i]) * (idx - means[i]))

    variances[i] = currSum / entries[i]
    stdevs[i] = math.sqrt(variances[i])

    confidence[i] = stdtrit(entries[i] - 1, 1 - 0.05)


# uncomment the following to print individual histogram graphs

# print("Handoffs")
# for i in range(numThreads):
#     print(handoffs[i])
# print("Histogram Stats")
# for i in range(numThreads):
#     print("Thd: " + str(i))
#     # print("Len: " + str(len(histogram[i])))
#     # print("Num Filled: " + str(numFilled[i]))
#     print("Mean: " + str(means[i]))
#     print("95% confidence: " + str(confidence[i]))
#     print("Stdev: " + str(stdevs[i]))
#     print("Median: " + str(medians[i]))
#     print("75 percentile: " + str(percentile75[i]))
#     print("90 percentile: " + str(percentile90[i]))
#     print("99 percentile: " + str(percentile99[i]))

# uncomment the following to generate histogram graphs

# Uncomment and change array slices to generate graphs
# print("Histogram")
# for i in range(numThreads):
#     plt.plot(histogram[i][0:int(2*stdevs[i])])
#     plt.savefig("thd" + str(i) + ".png")
#     plt.clf()
#     plt.plot(histogram[i][10:int(4*stdevs[i])])
#     plt.savefig("thd_tail" + str(i) + ".png")
#     plt.clf()
#     plt.plot(cdf_arr[i][0:int(2*stdevs[i])])
#     plt.savefig("cdf_thd" + str(i) + ".png")
#     plt.clf()

# write out data as a cdf

for i in range(numThreads):
    f = os.path.join(writeDirName, str(i))
    cdfWrite = open(f, "w", buffering=100000)
    for j in cdf_arr[i]:
        cdfWrite.write(str(j)+ "\n")
    cdfWrite.close()