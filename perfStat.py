import json
import sys
import numpy as np
from pathlib import Path
import scipy.stats as st
import matplotlib.pyplot as plt

def confidenceInterval(data):
	# Suppose n < 30
	# return st.t.interval(confidence=0.95, df=len(data)-1, loc=np.mean(data), scale=st.sem(data)) 
	# Suppose n >= 30
	return st.norm.interval(confidence=0.95, loc=np.mean(data), scale=st.sem(data))

# Append to existing stats
def parsePerfStat(stat, text: str):
	items = map(lambda l: json.loads(l), text.splitlines())
	for i in items:
		if i["event"] not in stat:
			stat[i["event"]] = {
				"values": [i["metric-value"]],
				"unit": i["metric-unit"]
			}
		else:
			stat[i["event"]]["values"].append(i["metric-value"])

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print("Usage: perfStat.py <statDir1> <statDir2> ...")
		sys.exit(1)

	dirs = sys.argv[1:]
	stats = []
	for d in dirs:
		stat = {}
		for f in Path(d).iterdir():
			if f.is_file():
				parsePerfStat(stat, f.read_text())
			
		for v in stat.values():
			v["ci"] = confidenceInterval(v["values"])
		
		stats.append(stat)

	# Events to plot
	events = ["cycles:u", "branches:u", "L1-dcache-loads:u", "LLC-loads:u"]

	cmap = plt.get_cmap("tab10")
	plt.xticks(range(len(events)), events)

	for i, e in enumerate(events):
		min_value = None
		for j, s in enumerate(stats):
			# find min value in the first round
			if min_value is None or stats[j][e]["ci"][0] < min_value:
				min_value = stats[j][e]["ci"][0]

		for j, s in enumerate(stats):
			# normalize
			ci = s[e]["ci"] / min_value
			# ci = s[e]["ci"]
			# Set label only once
			if i == 0:
				label = dirs[j]
			else:
				label = None
			plt.errorbar(i + j *0.04, np.mean(ci), yerr=ci[1]-np.mean(ci), capsize=2, color=cmap(j), label=label)

	plt.legend()
	plt.xlabel("Perf Stat")
	plt.ylabel("Normalized Value")
	plt.savefig("perf.png", dpi=400)
	
