import os
import sys
import time
import matplotlib.pyplot as plt
import math
from scipy.special import stdtrit
from scipy import stats
import numpy as np

if len(sys.argv) != 5:
    print("Not enough args")
    print("Usage: combineCDFs.py DirPrefix NumThreads NumToCombine WriteDir")
    sys.exit(-1)

dir_prefix = sys.argv[1]
numThreads = int(sys.argv[2])
numToCombine = int(sys.argv[3])
writeDirName = sys.argv[4]

def readInArr(file, arr):
    maxVal = 0
    while True:
        line = file.readline()
        if not line:
            break
        currValue = int(line)
        arr.append(currValue)
        maxVal = currValue

cdf_arr = [[0.0] for j in range(numThreads)]

for i in range(numToCombine):
    for k in range(numThreads):
        print(str(i) + ", " + str(k))
        curr_arr = []
        readFile = open(os.path.join(dir_prefix + str(i), str(k)), "r")
        readInArr(readFile, curr_arr)
        readFile.close()

        while(len(curr_arr) >= len(cdf_arr[k])):
            cdf_arr[k].append(cdf_arr[k][-1])

        for idx in range(max(len(curr_arr), len(cdf_arr[k]))):
            if idx < len(curr_arr):
                cdf_arr[k][idx] += curr_arr[idx]/numToCombine
            else:
                cdf_arr[k][idx] += curr_arr[-1]/numToCombine


# for i in range(numThreads):
#     plt.plot(cdf_arr[i][0:50])
#     plt.savefig("cdf_comb" + str(i) + ".png")
#     plt.clf()

writeDir = os.mkdir(writeDirName)

for i in range(numThreads):
    f = os.path.join(writeDirName, str(i))
    cdfWrite = open(f, "w")
    for j in cdf_arr[i]:
        cdfWrite.write(str(int(j))+ "\n")
    cdfWrite.close()