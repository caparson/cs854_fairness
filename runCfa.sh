#!/bin/sh -
~/cfa-cc/driver/cfa -quiet -g -O3 -nodebug -DNDEBUG spinCount.cfa -o spinCount

i=0 #number of samples
while [ ${i} -le 29 ] ; do
        for k in 0 1 2 3 4 5 10 20 50 100 200 500 1000
        do
                for j in 0 1 2 3 4 5 10 20 50 100 200 500 1000
                do
                        taskset -c 48-51 ./spinCount 4 4 10 $k $j
                        echo $k'_'$j'_'$i
                        python3 genStats.py 'trace' 4 'cfa_cdf_'$k'_'$j'_'$i
                done
        done
        i=`expr ${i} + 1`
done

# we ran getFairnessStats.py and genGraphs.py by hand afterwards. (They have variables that we changed by hand)