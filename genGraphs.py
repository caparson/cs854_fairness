import os
import sys
import time
import matplotlib.pyplot as plt
import math
from scipy.special import stdtrit
import scipy.stats as st
import numpy as np
import random
from dataclasses import dataclass
import pandas as pd

prefix = 'cfa_'

numThreads = 4

meanName = prefix + 'mean_'
confName = prefix + 'conf_'

means = []
confs = []

meanJain = pd.read_csv(meanName + 'jain.csv')
confJain = pd.read_csv(confName + 'jain.csv')

for i in range(numThreads):
    means.append(pd.read_csv(meanName + str(i) + '.csv'))
    confs.append(pd.read_csv(confName + str(i) + '.csv'))

# for highlighting confidence intervals that don't overlap with the initial interval
def noOverlap(y1, y2, y3, y4):
    return y3 > y2 or y4 < y1

cutoff = 1000 # cutoff if you don't want values higher than this on the x axis

exact = 0 # -1 for no exact match

xAxis = 0 # 0 for NCS 1 for CS

myLabels = []
for idx, row in means[0].iterrows():
    if row[0] > cutoff or row[1] > cutoff:
        continue

    if row[xAxis] != exact and exact != -1:
        continue
    
    myLabels.append(str(int(row[1 if xAxis == 0 else 0])))

counter=0

fig, ax = plt.subplots()

for idx, row in meanJain.iterrows():
    if row[0] > cutoff or row[1] > cutoff:
        continue
    
    if row[xAxis] != exact and exact != -1:
        continue

    plt.errorbar(counter, row[2], [[row[2] - confJain.iloc[idx][2]], [confJain.iloc[idx][3] - row[2]]], fmt='none', capsize=2)
    counter += 1

ax.set_xticks(list(range(counter)))
ax.set_xticklabels(myLabels)
plt.subplots_adjust(left=0.2,bottom=.2)
ax.set_xlabel("NCS Delay", fontsize=12)
plt.setp(ax.get_xticklabels(), rotation=70, horizontalalignment='right')
plt.title("Jain Index Confidence vs. NCS Delay with " + str(exact) + " CS Delay")

plt.ylabel("Jain Index")

plt.savefig(prefix + "jain.png")
plt.clf()

def getName(name):
    if name == "Per90":
        return "90th Percentile"
    if name == "Per99":
        return "99th Percentile"
    if name == "Per999":
        return "99.9th Percentile"
    return name


for i in range(numThreads):
    currMean = means[i]
    currConf = confs[i]

    graphs = ["Mean", "Median", "Per90", "Per99", "Per999", "Variance"]

    for graphIdx, graphName in enumerate(graphs):
        fig, ax = plt.subplots()
        counter=0
        currLoc = 2 + graphIdx
        y1 = 0
        y2 = 0
        first = True
        currConfLoc = 2 + graphIdx * 2
        for idx, row in currMean.iterrows():
            
            if row[0] > cutoff or row[1] > cutoff:
                continue
            
            if row[xAxis] != exact and exact != -1:
                continue

            if first:
                first = False
                y1 = currConf.iloc[idx][currConfLoc]
                y2 = currConf.iloc[idx][currConfLoc + 1]

            y3 = currConf.iloc[idx][currConfLoc]
            y4 = currConf.iloc[idx][currConfLoc + 1]
            plt.errorbar(counter, row[currLoc], [[row[currLoc] - currConf.iloc[idx][currConfLoc]], [currConf.iloc[idx][currConfLoc + 1] - row[currLoc]]], fmt='none', capsize=2, color= 'red' if noOverlap(y1, y2, y3, y4) else 'royalblue')
            counter += 1
        
        ax.set_xticks(list(range(counter)))
        ax.set_xticklabels(myLabels)
        ax.set_xlabel("NCS Delay" if xAxis == 0 else 'CS Delay', fontsize=12)
        plt.setp(ax.get_xticklabels(), rotation=70, horizontalalignment='right')
        plt.title(getName(graphName) + " Stretch Confidence vs. NCS Delay with " + str(exact) + (" CS Delay" if xAxis == 0 else ' NCS Delay'))
        
        plt.ylabel(getName(graphName))

        # add this back in if you want to change the y scale
        # plt.ylim(ymax = 10, ymin = 0)
        plt.subplots_adjust(bottom=.2)
        fig.savefig(prefix + graphName + "_" + str(i) + ".png")
        fig.clf()