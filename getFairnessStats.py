import os
import sys
import time
import matplotlib.pyplot as plt
import math
from scipy.special import stdtrit
import scipy.stats as st
import numpy as np
import random
from dataclasses import dataclass

def readInArr(file, arr):
    while True:
        line = file.readline()
        if not line:
            break
        currValue = int(line)
        arr.append(currValue)

# NCS and CS delays we tested
in_delays = [0, 1, 2, 3, 4, 5, 10, 20, 50, 100, 200, 500, 1000]
out_delays = [0, 1, 2, 3, 4, 5, 10, 20, 50, 100, 200, 500, 1000]

#number of threads in data
numThreads = 4

#number of samples collected
numSamples = 30

def getConfInterval(arr):
    return st.t.interval(alpha=0.95, df=len(arr)-1, loc=np.mean(arr), scale=st.sem(arr))

def nanToZero(val):
    if math.isnan(val):
        return 0
    return val

class Entry:
    def __init__(self):
        self.INDELAY = []
        self.OUTDELAY = []
        self.Mean = []
        self.Median = []
        self.Per90 = []
        self.Per99 = []
        self.Per999 = []
        self.Variance = []


    def addFields(self, in_d, out_d, mean, median, per90, per99, per999, variance):
        self.INDELAY.append(in_d)
        self.OUTDELAY.append(out_d)
        self.Mean.append(mean)
        self.Median.append(median)
        self.Per90.append(per90)
        self.Per99.append(per99)
        self.Per999.append(per999)
        self.Variance.append(variance)

    def getRow(self):
        return str(self.INDELAY[0]) + ", " + \
            str(self.OUTDELAY[0]) + ", " + \
            str(np.mean(self.Mean)) + ", " + \
            str(np.mean(self.Median)) + ", " + \
            str(np.mean(self.Per90)) + ", " + \
            str(np.mean(self.Per99)) + ", " + \
            str(np.mean(self.Per999)) + ", " + \
            str(np.mean(self.Variance)) + "\n"
    
    def getConfRow(self):
        mean_conf = getConfInterval(self.Mean)
        med_conf = getConfInterval(self.Median)
        p90_conf = getConfInterval(self.Per90)
        p99_conf = getConfInterval(self.Per99)
        p999_conf = getConfInterval(self.Per999)
        var_conf = getConfInterval(self.Variance)

        return str(self.INDELAY[0]) + ", " + str(self.OUTDELAY[0]) + ", " + \
            str(nanToZero(mean_conf[0])) + ", " + str(nanToZero(mean_conf[1])) + ", " + \
            str(nanToZero(med_conf[0])) + ", " + str(nanToZero(med_conf[1])) + ", " + \
            str(nanToZero(p90_conf[0])) + ", " + str(nanToZero(p90_conf[1])) + ", " + \
            str(nanToZero(p99_conf[0])) + ", " + str(nanToZero(p99_conf[1])) + ", " + \
            str(nanToZero(p999_conf[0])) + ", " + str(nanToZero(p999_conf[1])) + ", " + \
            str(nanToZero(var_conf[0])) + ", " + str(nanToZero(var_conf[1])) + "\n"


class JainEntry:
    def __init__(self):
        self.INDELAY = []
        self.OUTDELAY = []
        self.Jain_Index = []

    def addFields(self, in_d, out_d, jIndex):
        self.INDELAY.append(in_d)
        self.OUTDELAY.append(out_d)
        self.Jain_Index.append(jIndex)
    
    def getRow(self):
        return str(self.INDELAY[0]) + ", " + \
            str(self.OUTDELAY[0]) + ", " + \
            str(np.mean(self.Jain_Index)) + "\n"

    def getConfRow(self):
        jain_conf = getConfInterval(self.Jain_Index)


        return str(self.INDELAY[0]) + ", " + str(self.OUTDELAY[0]) + ", " + \
            str(jain_conf[0]) + ", " + str(jain_conf[1]) + "\n"


allData = [[[Entry() for k in range(numThreads)] for i in out_delays] for j in in_delays]

jainData = [[JainEntry() for i in out_delays] for j in in_delays]


currArr = []

jain_numer = 0
jain_denom = 0

mean = 0
median = -1
Per90 = -1
Per99 = -1
Per999 = -1
variance = 0

last = 0
weightedSum = 0

# directory prefix of cdfs to read from
prefix = 'cfa_cdf_'

for sample in range(numSamples):
    print(str(sample))
    # if sample < 8:
    #     continue
    for idx_in, in_d in enumerate(in_delays):
        for idx_out, out_d in enumerate(out_delays):
            for i in range(numThreads):
                # print(str(in_d) + "_" +str(out_d) + "_" +str(sample)+ "/ " +str(i))
                currDataArr = allData[idx_in][idx_out][i]

                readFile = open(os.path.join(prefix + str(in_d) + "_" + str(out_d) + "_" + str(sample), str(i)), "r")
                readInArr(readFile, currArr)
                readFile.close()

                for idx, val in enumerate(currArr):
                    if (val - last) > 0:
                        weightedSum += (val - last) * idx
                    last = val

                    if val > currArr[-1] * 0.5 and median == -1:
                        median = idx

                    if val > currArr[-1] * 0.9 and Per90 == -1:
                        Per90 = idx

                    if val > currArr[-1] * 0.99 and Per99 == -1:
                        Per99 = idx

                    if val > currArr[-1] * 0.999 and Per999 == -1:
                        Per999 = idx

                mean = weightedSum / currArr[-1]

                #variance

                weightedSum = 0
                last = 0
                for idx, val in enumerate(currArr):
                    weightedSum += (val - last) * ((idx - mean) * (idx - mean))
                    last = val

                variance = weightedSum / currArr[-1]

                jain_numer += currArr[-1]
                jain_denom += currArr[-1] * currArr[-1]

                # writeFiles[i].write(str(in_d) + ", " + str(out_d) + ", " + str(mean) + ", " + str(median) + ", " + str(Per90) + ", " + str(Per99) + ", " + str(Per999) + ", " + str(variance) + "\n")
                #print(str(in_d) + ", " + str(out_d) + ", " + str(mean) + ", " + str(median) + ", " + str(Per90) + ", " + str(Per99) + ", " + str(Per999) + ", " + str(variance) + "\n")
                currDataArr.addFields(in_d, out_d, mean, median, Per90, Per99, Per999, variance)

                #reset
                currArr.clear()
                mean = 0
                median = -1
                Per90 = -1
                Per99 = -1
                Per999 = -1
                var = 0

                last = 0
                weightedSum = 0
            
            jain_numer = jain_numer * jain_numer
            jain_denom = jain_denom * numThreads
            # jain_file.write(str(in_d) + ", " + str(out_d) + ", " + str(jain_numer/jain_denom) + "\n")
            jainData[idx_in][idx_out].addFields(in_d, out_d, jain_numer/jain_denom)

            #reset
            jain_numer = 0
            jain_denom = 0

write_prefix = "cfa"

writeFiles = []
confWriteFiles = []
for i in range(numThreads):
    writeFiles.append(open(write_prefix + "_mean_" + str(i) + ".csv", "w"))
    confWriteFiles.append(open(write_prefix + "_conf_" + str(i) + ".csv", "w"))
    writeFiles[i].write("INDELAY, OUTDELAY, Mean, Median, Per90, Per99, Per999, Variance\n")
    confWriteFiles[i].write("INDELAY, OUTDELAY, Mean_lo, Mean_hi, Median_lo, Median_hi, Per90_lo, Per90_hi, Per99_lo, Per99_hi, Per999_lo, Per999_hi, Variance_lo, Variance_hi\n")


jain_file = open(write_prefix + "_mean_jain.csv", "w")
conf_jain_file = open(write_prefix + "_conf_jain.csv", "w")
jain_file.write("INDELAY, OUTDELAY, Jain_Index\n")
conf_jain_file.write("INDELAY, OUTDELAY, Jain_Index_lo, Jain_Index_hi\n")

print("writing")
for idx_in, in_d in enumerate(in_delays):
    for idx_out, out_d in enumerate(out_delays):
            for i in range(numThreads):
                writeFiles[i].write(allData[idx_in][idx_out][i].getRow())
                confWriteFiles[i].write(allData[idx_in][idx_out][i].getConfRow())

for idx_in, in_d in enumerate(in_delays):
    for idx_out, out_d in enumerate(out_delays):
        jain_file.write(jainData[idx_in][idx_out].getRow())
        conf_jain_file.write(jainData[idx_in][idx_out].getConfRow())