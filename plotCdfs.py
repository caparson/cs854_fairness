import os
import sys
import time
import matplotlib.pyplot as plt
import math
from scipy.special import stdtrit
from scipy import stats
import numpy as np

numThreads = int(sys.argv[1])

def readInArr(file, arr):
    maxVal = 0
    while True:
        line = file.readline()
        if not line:
            break
        currValue = int(line)
        arr.append(currValue)
        maxVal = currValue

cdf_arr = [[[0.0] for j in range(numThreads)] for i in range(len(sys.argv) - 2)]


for i in range(2, len(sys.argv)):
    for k in range(numThreads):
        curr_arr = cdf_arr[i - 2][k]
        readFile = open(os.path.join(sys.argv[i], str(k)), "r")
        readInArr(readFile, curr_arr)
        readFile.close()

        for idx, val in enumerate(curr_arr):
            curr_arr[idx] = val / curr_arr[-1]
            


for k in range(numThreads):
    for i in range(2, len(sys.argv)):
        plt.plot(cdf_arr[i - 2][k][0:10])
    plt.savefig("cdf_comb" + str(k) + ".png")
    plt.clf()
