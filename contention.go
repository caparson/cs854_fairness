package main

import (
	"bufio"
	"fmt"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"
)

func printUsage() {
	fmt.Println("Usage: ./contention NumThread Duration InDelay OutDelay")
	os.Exit(-1)
}

func main() {
	N, err := strconv.Atoi(os.Args[1])
	if err != nil {
		printUsage()
	}
	DURATION, err := strconv.Atoi(os.Args[2])
	if err != nil {
		printUsage()
	}
	inDelay, err := strconv.Atoi(os.Args[3])
	if err != nil {
		printUsage()
	}
	outDelay, err := strconv.Atoi(os.Args[4])
	if err != nil {
		printUsage()
	}

	const ITER = 1000000000

	globalCounter := 0
	globalSignal := 0

	runtime.GOMAXPROCS(N)

	sequence := make([]int, ITER)

	var mtx sync.Mutex
	var wg sync.WaitGroup
	// start := time.Now()

	for i := 0; i < N; i++ {
		wg.Add(1)
		go func(n int) {
			for {
				mtx.Lock()
				cnt := globalCounter
				globalCounter++
				for j := 0; j < inDelay; j++ {
				}
				mtx.Unlock()
				// if globalCounter >= ITER {
				// 	break
				// }
				if globalSignal == 1 {
					break
				}
				sequence[cnt] = n
				for j := 0; j < outDelay; j++ {
				}
			}
			wg.Done()
		}(i)
	}

	time.Sleep(time.Second * time.Duration(DURATION))
	globalSignal = 1

	wg.Wait()

	f, err := os.Create("./trace_go")
	defer f.Close()
	if err != nil {
		panic(err)
	}
	bufStdout := bufio.NewWriter(f)
	defer bufStdout.Flush()

	for i := 0; i < globalCounter; i++ {
		fmt.Fprintln(bufStdout, sequence[i])
	}
}
