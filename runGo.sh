#!/bin/sh -
go build ./contention.go


i = 0 # number of samples
while [ ${i} -le 29 ] ; do
        for k in 0 1 5 10 20 50 100 200 500 1000
        do
                for j in 0 1 5 10 20 50 100 200 500 1000
                do
                        taskset -c 48-51 ./contention 4 10 $k $j
                        echo $k'_'$j'_'$i
                        python3 genStats.py 'trace_go' 4 'go_cdf_'$k'_'$j'_'$i
                done
        done
    i=`expr ${i} + 1`
done

# we ran getFairnessStats.py and genGraphs.py by hand afterwards. (They have variables that we changed by hand)