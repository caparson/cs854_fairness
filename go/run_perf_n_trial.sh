#!/bin/bash

DIR=$(dirname $0)
cd $DIR

if [ $# -ne 2 ]; then
	echo "Usage: run_perf_n_trial.sh <num> <dir_prefix>"
	exit 1
fi

for i in $(seq 1 $1); do
	./run_perf.sh 30 "$2$i"
done
