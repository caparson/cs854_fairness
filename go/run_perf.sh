#!/bin/sh
set -e

if [ $# -ne 2 ]; then
	echo "Usage: run_perf.sh <num> <data_dir>"
	exit 1
fi

cd $(dirname $0)
go build ./contention_perf.go

PROGRAM=./contention_perf
DATA_DIR=$2

mkdir -p $DATA_DIR

NUM=$1
i=0
while [ ${i} -lt $NUM ]; do
	echo "Round $i..."
	perf stat -j -dB taskset -c 0-3 $PROGRAM 4 10 &> $DATA_DIR/stat${i}.json
	i=`expr ${i} + 1`
done
