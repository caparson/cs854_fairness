#!/bin/sh
set -e

if [ $# -ne 1 ]; then
	echo "Usage: exp.sh <num>"
	exit 1
fi

cd $(dirname $0)
go build ./contention.go

PROGRAM=./contention
DATA_DIR=data

mkdir -p $DATA_DIR

NUM=$1
i=0
while [ ${i} -lt $NUM ]; do
	echo "Round $i (1)..."
	taskset -c 0-3 $PROGRAM 4 10 0 0
	python ../genStats.py trace.txt 4 "$DATA_DIR/cdf_$i"
	echo "Round $i (2)..."
	taskset -c 0-3 $PROGRAM 4 10 0 0
	python ../genStats.py trace.txt 4 "$DATA_DIR/cdf_2_$i"
	i=`expr ${i} + 1`
done

python3 ../combineCDFs.py "$DATA_DIR/cdf_" 4 $NUM "$DATA_DIR/test_cdf"
python3 ../combineCDFs.py "$DATA_DIR/cdf_2_" 4 $NUM "$DATA_DIR/test_cdf_2"
python3 ../KStest.py "$DATA_DIR/test_cdf" "$DATA_DIR/test_cdf_2" 4
