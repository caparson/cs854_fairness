import os
import sys
import time
import matplotlib.pyplot as plt
import math
from scipy.special import stdtrit
from scipy import stats
import numpy as np

if len(sys.argv) < 4 or len(sys.argv) > 5:
    print("Not enough args")
    print("Usage: KStest.py Dir1 Dir2 Numthreads")
    sys.exit()

dir1 = sys.argv[1]
dir2 = sys.argv[2]
numThreads = int(sys.argv[3])

BIN = 10 #number of elements on the tail to bin (limit very long sparse tail error)

def calcKS(cdf1, cdf2):
    D_ks = []
    for i in range(max(len(cdf1), len(cdf2))):
        currDiff = 0
        if i >= len(cdf1):
            D_ks.append(abs(1 - cdf2[i]))
        elif i >= len(cdf2):
            D_ks.append(abs(cdf1[i] - 1))
        else:
            D_ks.append(abs(cdf1[i] - cdf2[i]))

    ks_stat = max(D_ks)
    max_idx = np.argmax(D_ks)
    print("max idx: " + str(max_idx))
    if max_idx >= len(cdf2):
        print(1)
    else:
        print(cdf2[max_idx])
    if max_idx >= len(cdf1):
        print(1)
    else:
        print(cdf1[max_idx])
    print("diff: " + str(D_ks[max_idx]))
    m, n = float(len(cdf1)), float(len(cdf2))
    print("lens " + str(len(cdf1)) + ", " + str(len(cdf2)))
    en = m * n / (m + n)
    p_value = stats.kstwo.sf(ks_stat, np.round(en))
    return p_value

def readInArr(file, arr):
    maxVal = 0
    while True:
        line = file.readline()
        if not line:
            break
        currValue = int(line)
        arr.append(currValue)

    maxVal = arr[-1]
    for idx, elem in reversed(list(enumerate(arr))):
        if maxVal - elem > BIN:
            break
        arr.pop()

    maxVal = arr[-1]
    for idx, val in enumerate(arr):
        arr[idx] = val/maxVal

    arr[-1] += BIN/maxVal

p_values = []

cdf_arr1 = []
cdf_arr2 = []
for i in range(numThreads):
    # print("thd " + str(i))
    readFile = open(os.path.join(dir1, str(i)), "r")
    readInArr(readFile, cdf_arr1)
    readFile.close()

    readFile = open(os.path.join(dir2, str(i)), "r")
    readInArr(readFile, cdf_arr2)
    readFile.close()


    p_values.append(calcKS(cdf_arr1, cdf_arr2))
    cdf_arr1.clear()
    cdf_arr2.clear()
    
for i in range(numThreads):
    print("thd " + str(i) + " p value: " + str(p_values[i]))